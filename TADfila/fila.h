#define tamanho 6

struct fila{
  int fila[tamanho];
  int inicio, fim;
};
typedef struct fila Fila;

void inicializa(Fila *f){
  f->inicio=0;
  f->fim=-1;
};

void insere(Fila *f, int elem){
 if(f->fim == tamanho-1){
    printf("Fila cheia\n\n");
 }else{
   f->fim++;
   f->fila[f->fim] = elem;
 }
};

void retira(Fila *f){
  if(f->fim < f->inicio){
    printf("Fila Vazia\n\n");
  }else{
    f->inicio++;
  }
};

void imprimefila(Fila *f){
 printf("%d", tamanho);
};


/*void imprimefim(Fila *f);
void imprimeinicio(Fila *f);*/
