#include <stdio.h>
#include <stdlib.h>
#include "fila.h"

int main(){

  Fila x;
  inicializa(&x);

  insere(&x,3);
  insere(&x,7);
  insere(&x,1);

  imprimefila(&x);

  insere(&x,5);
  insere(&x,9);
  insere(&x,2);

  imprimefila(&x);

  retira(&x);
  retira(&x);

  imprimefila(&x);
}
